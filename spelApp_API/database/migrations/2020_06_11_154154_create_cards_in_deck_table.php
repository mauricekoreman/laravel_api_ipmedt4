<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsInDeckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards_in_deck', function (Blueprint $table) {
            $table->integer('deck_id')->unsigned();
            $table->foreign('deck_id')->references('deck_id')->on('decks');
            $table->string('deck_name', 30);
            $table->integer('card_id')->unsigned();
            $table->foreign('card_id')->references('card_id')->on('cards'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cards_in_deck', function($table) {
            $table->dropForeign('cards_in_deck_deck_id_foreign');
        });

        Schema::table('cards_in_deck', function($table) {
            $table->dropForeign('cards_in_deck_card_id_foreign');
        });

        Schema::dropIfExists('cards_in_deck');
    }
}
