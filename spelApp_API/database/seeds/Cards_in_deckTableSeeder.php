<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Cards_in_deckTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 52; $i++) {
            DB::table('cards_in_deck')->insert([
                [
                    'deck_id' => 1,
                    'deck_name' => 'Demarreren',
                    'card_id' => $i,
                ],
            ]);
        }

    }
}
