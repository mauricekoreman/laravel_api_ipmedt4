<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_name' => 'Spades',
            ],
            [
                'category_name' => 'Hearts',
            ],
            [
                'category_name' => 'Clubs',
            ],
            [
                'category_name' => 'Diamonds',
            ],
        ]);
    }
}
