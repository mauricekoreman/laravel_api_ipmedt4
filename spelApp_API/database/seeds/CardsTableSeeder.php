<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"];
        $count = 1;

        for($i = 1; $i <= 4; $i++) {
            if ($i == 1) {
                $category = "Spades";
            } elseif ($i == 2) {
                $category = "Hearts";
            } elseif ($i == 3) {
                $category = "Clubs";
            } else {
                $category = "Diamonds";
            } 

            for ($x = 0; $x < 13; $x++) {
                DB::table('cards')->insert([
                    'category_id' => $i,
                    'card_value' => ($x+1),
                    'card_name' => $names[$x] . ' of ' . $category,
                    'card_image' => '/card_img/' . ($x+1) . $category[0] . '.png',
                ]);
                $count++;
            }
        }
    }
}
