<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DecksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('decks')->insert([
            [
                'deck_id' => 0,
                'game_name' => "Demarreren",
            ],
        ]);
    }
}
