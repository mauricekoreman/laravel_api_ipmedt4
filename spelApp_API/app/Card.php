<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'cards';

    public function getCategory() {
        return $this->hasOne('App\Category', 'category_id', 'category_id');
    }
}
