<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public static $wrap = 'cards';

    public function toArray($request)
    {
        
        return [
           'card_id' => $this->card_id,
           'category_id' => $this->category_id,
           'card_value' => $this->card_value,
           'card_name' => $this->card_name,
           'card_image' => $this->card_image,
       ];
    }
}
