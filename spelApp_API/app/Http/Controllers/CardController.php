<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use Illuminate\Support\Facades\DB;

use App\Http\Resources\CardResource as CardResource;


class CardController extends Controller
{
    public function show($card_id) {
        $card = Card::where('card_id', '=', $card_id)->first();
        $category_of_card = $card->getCategory;

        return $card;
    }

    public function index() {
        // $cards = [DB::table('cards')->get()];
        // $card_list = [$cards];

        $cards = Card::get();

        // return $card_list;
        return CardResource::collection($cards);
        // return $cards;
    }
}